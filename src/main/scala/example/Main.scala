package example

def times2(i: Int) = i * 2;

def subtract2(i: Int):Int = {
  i - 2;
}

def sayHello = println("Hello")

def myRun(): Unit = {
  val myVal = 10

  val myVal2 = {
    val a = 5;
    a + 3
    val b = 2;
    b
  }

  println(myVal2)
}


class Employee(val fname: String, val lname:String, var age: Int = 0) {
  def setAge(newAge: Int): Int = {
    age = newAge
    age
  }
}

val yoofi = new Employee("Yoofi", "Brown-Pobee", 26)

@main def mymain(): Unit = {
  println(yoofi.age)
  yoofi.setAge(12)
  println(yoofi.age)
}

def run() = {
  val ages = Seq(42, 61, 29, 64)
  println(s"The oldest person is ${ages.max}")
  myRun()
  var myNum = 10
  println(myNum)
  myNum = subtract2(myNum)
  println(myNum)
  sayHello
  myNum match {
    case 1 => println(1)
    case _ => println(10)
  }

  val ** = 2
  val `richmond` = "hello"
  val `bernard.tk` = "b"

  println(`richmond`)
  println(`bernard.tk`)
  println(**)
  var i = 0
  for (i <- 1 to 10) {
    print(i)
  }
}

object HelloWorld {
  def main(args: Array[String]): Unit = {

  }
}
