package lab2

class Trade(val id: String, val symbol: String, private val quantity: Int, private var price: Int) {
  def this(id: String, symbol: String, quantity: Int) = this(id, symbol, quantity, 0)

  def getQuantity: Int = quantity;

  def setPrice(newPrice: Int): Unit = {
    if(newPrice > 0) price = newPrice
  }

  def getPrice: Int = price

  def getValue: Int = price * quantity

}
