package lab1

def celciusToFahrenheit(celcius: Int): Int = (celcius * 9/5) + 32

def expandDFate(shortForm: String): String = {
  val dateDivisions = shortForm.split("/")
  val day = dateDivisions(0)
  val month = dateDivisions(1)
  val year = dateDivisions(2)
  s"${convertDayToDate(day)} ${convertMonthToWords(month)} $year"
}

def convertDayToDate(dateDigit: String): String = {
  val suffix = {
    if(dateDigit.endsWith("1")) "st"
    else if (dateDigit.endsWith("2")) "nd"
    else if (dateDigit.endsWith("3")) "rd"
    else "th"
  }

  if(dateDigit.charAt(dateDigit.length - 2) == '0') {
    dateDigit.charAt(dateDigit.length - 1) + suffix
  } else {
    dateDigit + suffix
  }
}

def convertMonthToWords(monthDigit: String): String = {
  monthDigit match {
    case "01" => "January"
    case "02" => "February"
    case "03" => "March"
  }
}

def extractFields(line: String) = {

}

@main def run(): Unit = {
  println(celciusToFahrenheit(37))
  println(expandDFate("05/03/2022"))
}